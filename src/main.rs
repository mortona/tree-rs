use std::fs;
use std::fs::ReadDir;

mod config;
use crate::config::Config;

fn main() {
    let config = Config::new();
    let dir = fs::read_dir(&config.dir).unwrap();
    let mut report = Report::new();
    display_dir(dir, 0, 0, &mut report, &config);
    if config.include_report {
        report.display();
    }
}

struct Report {
    dir_count: u32,
    file_count: u32,
}

impl Report {
    fn new() -> Report {
        Report {
            dir_count: 0,
            file_count: 0,
        }
    }

    fn inc_file(&mut self) {
        self.file_count += 1;
    }

    fn inc_dir(&mut self) {
        self.dir_count += 1;
    }

    fn display(&self) {
        println!("");
        println!("{} directories, {} files", self.dir_count, self.file_count);
    }
}

fn display_dir(dir: ReadDir, indent: usize, level: u32, report: &mut Report, config: &Config) {
    if Some(level) == config.dir_level_limit {
        return;
    }

    for e in dir {
        let e = e.unwrap();
        let p = e.path();

        let postfix = if p.is_dir() { "/" } else { "" };

        if !p.is_dir() && config.dirs_only {
            continue;
        }

        let entry_name = e.file_name().into_string().unwrap();
        if !entry_name.starts_with(".") || config.show_hidden {
            println!("{}{}{}", " ".repeat(indent), entry_name, postfix);

            if p.is_dir() {
                report.inc_dir();
                display_dir(
                    fs::read_dir(p).unwrap(),
                    indent + config.indent_level,
                    level + 1,
                    report,
                    config,
                );
            } else {
                report.inc_file();
            }
        }
    }
}
