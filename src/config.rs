extern crate clap;
use clap::{App, Arg};

pub struct Config {
    pub dir: String,
    pub dirs_only: bool,
    pub show_hidden: bool,
    pub dir_level_limit: Option<u32>,
    pub include_report: bool,
    pub indent_level: usize,
}

impl Config {
    pub fn new() -> Config {
        let matches = App::new("tree-rs")
            .version("0.0.1")
            .author("Alex Morton <mortonar01@gmail.com>")
            .about("A 'tree' utility written in Rust!")
            .arg(
                Arg::with_name("d")
                    .short("d")
                    .takes_value(false)
                    .required(false),
            )
            .arg(
                Arg::with_name("a")
                    .short("a")
                    .takes_value(false)
                    .required(false),
            )
            .arg(
                Arg::with_name("L")
                    .short("L")
                    .takes_value(true)
                    .validator(Config::validate_length)
                    .required(false),
            )
            .arg(Arg::with_name("noreport").long("noreport").required(false))
            .arg(Arg::with_name("directory").required(true))
            .get_matches();

        Config {
            dir: matches.value_of("directory").unwrap().to_owned(),
            dirs_only: matches.is_present("d"),
            show_hidden: matches.is_present("a"),
            dir_level_limit: matches.value_of("L").map(|l| l.parse::<u32>().unwrap()),
            include_report: !matches.is_present("noreport"),
            indent_level: 4,
        }
    }

    pub fn validate_length(value: String) -> Result<(), String> {
        let error = "length must be an integer greater than 0".to_owned();
        if let Ok(value) = value.parse::<u32>() {
            if value > 0 {
                return Ok(());
            }
        }
        return Err(error);
    }
}
